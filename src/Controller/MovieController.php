<?php declare(strict_types=1);

namespace App\Controller;

use App\Entity\Movie;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use Slim\Interfaces\RouteCollectorInterface;
use Slim\Views\Twig;

class MovieController
{
    public function __construct(
        private RouteCollectorInterface $routeCollector,
        private Twig $twig,
        private EntityManagerInterface $em,
    ) {}


    public function index(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $movieRepo = $this->em->getRepository(Movie::class);

        return $this->twig->render($response, 'movie/index.html.twig', [
            'trailers' => $movieRepo->getLastN(10)
        ]);
    }

    /**
     *
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param array $args
     * @return ResponseInterface
     * @throws HttpNotFoundException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function detail(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $movie = $this->findOrFail($request, (int) $args['id']);

        return $this->twig->render($response, 'movie/detail.html.twig', [
            'trailer' => $movie
        ]);
    }

    /**
     * Find entity of fails with NotFound
     *
     * @param ServerRequestInterface $request
     * @param int $id
     * @return Movie
     * @throws HttpNotFoundException
     */
    protected function findOrFail(ServerRequestInterface $request, int $id) : Movie
    {
        /** @var Movie $movie */
        $movie = $this->em->getRepository(Movie::class)->find($id);
        if ($movie === null) {
            throw new HttpNotFoundException($request, "Movie with id=$id not found");
        }
        return $movie;
    }
}
