<?php declare(strict_types=1);

namespace App\Repository;

use App\Entity\Movie;
use Doctrine\ORM\EntityRepository;

class MovieRepository extends EntityRepository
{
    /**
     * @param int $count
     * @return array<Movie>
     */
    public function getLastN(int $count)
    {
        return $this->findBy([],['id' => 'desc'], 10);
    }
}
