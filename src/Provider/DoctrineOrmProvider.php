<?php declare(strict_types=1);

namespace App\Provider;

use App\Container\Container;
use App\Support\Config;
use App\Support\ServiceProviderInterface;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Tools\Setup;
use Psr\Container\ContainerInterface;

class DoctrineOrmProvider implements ServiceProviderInterface
{
    public function register(Container $container): void
    {
        $container->set(EntityManager::class, function (ContainerInterface $container): EntityManager {
            $config = $container->get(Config::class);

            $doctrineConfig = Setup::createAnnotationMetadataConfiguration($config->get('doctrine')['mapping'], ($_ENV['APP_ENV'] ?? 'dev') === 'dev');
            $doctrineConfig->setMetadataDriverImpl(new AnnotationDriver(new AnnotationReader(), $config->get('doctrine')['mapping']));
            $doctrineConfig->setMetadataCacheImpl(new FilesystemCache($config->get('base_dir') . '/var/cache/doctrine'));

            return EntityManager::create([
                'driver' =>  $_ENV['DB_DRIVER'] ?? $config->get('doctrine')['connection']['driver'] ?? null,
                'host' => $_ENV['DB_HOST'] ?? $config->get('doctrine')['connection']['host'] ?? null,
                'port' => $_ENV['DB_PORT'] ?? $config->get('doctrine')['connection']['port'] ?? null,
                'dbname' => $_ENV['DB_NAME'] ?? $config->get('doctrine')['connection']['dbname'] ?? null,
                'user' => $_ENV['DB_USER'] ?? $config->get('doctrine')['connection']['user'] ?? null,
                'password' => $_ENV['DB_PASSWORD'] ?? $config->get('doctrine')['connection']['password'] ?? null,
            ], $doctrineConfig);
        });

        $container->set(EntityManagerInterface::class, static function (ContainerInterface $container): EntityManagerInterface {
            return $container->get(EntityManager::class);
        });
    }
}
