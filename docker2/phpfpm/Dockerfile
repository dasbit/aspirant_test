FROM composer:latest as composer
FROM php:8.0-fpm-alpine

RUN set -xe \
        && apk add --no-cache \
           libzip-dev \
           libintl \
           icu \
           icu-dev \
           curl \
           libmcrypt \
           libmcrypt-dev \
           libxml2-dev \
           freetype \
           freetype-dev \
           libpng \
           libpng-dev \
           libjpeg-turbo \
           libjpeg-turbo-dev \
           postgresql-dev \
           pcre-dev \
           git \
           g++ \
           make \
           autoconf \
           openssh \
           util-linux-dev \
           libuuid \
           sqlite-dev \
           libxslt-dev

RUN docker-php-ext-install \
    zip \
    iconv \
    soap \
    sockets \
    intl \
    pdo_mysql \
    pdo_pgsql \
    exif \
    pcntl \
    xsl

ENV COMPOSER_ALLOW_SUPERUSER=1
ENV COMPOSER_MEMORY_LIMIT=-1
COPY --from=composer /usr/bin/composer /usr/local/bin/composer
RUN /usr/local/bin/composer self-update

ADD crontab.txt /crontab.txt
RUN /usr/bin/crontab /crontab.txt

ADD docker-php-entrypoint /usr/local/bin/docker-php-entrypoint
RUN chown -R root:root /usr/local/bin/docker-php-entrypoint
RUN chmod -R 755 /usr/local/bin/docker-php-entrypoint

